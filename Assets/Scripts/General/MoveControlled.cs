using UnityEngine;

public class MoveControlled : MonoBehaviour
{
    public float speed;
    public float force;
    public bool moveWithVelocity;
    public bool moveWithForce;

    private Rigidbody2D _rb;
    
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        moveWithVelocity = false;
        moveWithForce = false;
    }

    // Update is called once per frame
    void Update()
    {
        MoveWithVelocity();
        MoveWithForce();
    }

    void MoveWithVelocity()
    {
        if (moveWithVelocity)
        {
            _rb.velocity += new Vector2(
              speed * Time.deltaTime * Input.GetAxis("Horizontal"),
              speed * Time.deltaTime * Input.GetAxis("Vertical"));

            LimitVelocity();
        }
    }

    void MoveWithForce()
    {
        if (moveWithForce)
        {
            _rb.AddForce(new Vector2(
                force * Time.deltaTime * Input.GetAxis("Horizontal"),
                force * Time.deltaTime * Input.GetAxis("Vertical")));

            LimitVelocity();
        }
    }

    void LimitVelocity()
    {
        _rb.velocity = new Vector2(
            Mathf.Clamp(_rb.velocity.x, speed * -10, speed * 10),
            Mathf.Clamp(_rb.velocity.y, speed * -10, speed * 10));
    }
}
