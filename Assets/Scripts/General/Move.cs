using UnityEngine;

public class Move : MonoBehaviour
{
    private Rigidbody2D _rb;
    public float speed;

    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        _rb.velocity += new Vector2(Time.deltaTime * speed, 0);
    }
}
