using UnityEngine;

public class VelocityForceTest : MonoBehaviour
{
    public bool resetPosition;
    public Vector2 valuesToVelocity;
    public Vector2 valuesToForce;
    public bool jumpWithVelocity;
    public bool jumpWithForce;

    private Rigidbody2D _rb;

    void Start()
    {
        resetPosition = false;
        _rb = GetComponent<Rigidbody2D>();
        jumpWithVelocity = false;
        jumpWithForce = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q)) jumpWithVelocity = true;
        if (Input.GetKeyDown(KeyCode.E)) jumpWithForce = true;

        ResetPosition();
        JumpWithVelocity();
        JumpWithForce();
    }

    void ResetPosition() 
    {
        if (resetPosition)
        {
            _rb.velocity = Vector2.zero;
            _rb.transform.position = Vector2.zero;
            resetPosition = false;
        }
    }

    void JumpWithVelocity()
    {
        if (jumpWithVelocity)
        {
            _rb.velocity += valuesToVelocity;
            jumpWithVelocity = false;
        }
    }

    void JumpWithForce()
    {
        if (jumpWithForce)
        {
            _rb.AddForce(valuesToForce);
            jumpWithForce = false;
        }
    }
}
